""" Heuristic function for the 8-Queen Puzzle and its auxilliary functions """

def horizontal_attacks(table):
    """ Returns the number of queen pairs of queens where each one attacks
    each other horizontally in a given state """
    total = 0
    for i in range(len(table)):
        q1 = table[i]
        j = i+1
        while j < len(table):
            q2 = table[j]
            if q1 == q2:
                total += 1
            j += 1
    return total

def diagonal_attacks(table):
    """ Returns the number of queen pairs of queens where each one attacks
    each other diagonally in a given state """
    total = 0
    for i in range(len(table)):
        q1 = table[i]
        j = i+1
        k = 1
        while j < len(table):
            q2 = table[j]
            if q2==q1+k or q2==q1-k:
                total+=1
            k += 1
            j += 1
    return total

def h(state):
    """ Heuristic function for the 8-Queen Puzzle """
    return diagonal_attacks(state) + horizontal_attacks(state)        
