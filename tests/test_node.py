#!/usr/bin/python
import unittest
from problem import Node, Problem

class NodeTestCase(unittest.TestCase):

    def setUp(self):
        self.state = (1,1,1,1,1,1,1,1)
        self.path_cost = 0
        self.action = (0,5)
        self.problem = Problem(self.state)
        self.node = Node(self.state, self.path_cost)

    def test_node_init(self):
        node = Node(self.state, self.path_cost)
        self.assertEqual(node.state, self.state)
        self.assertEqual(node.path_cost, self.path_cost)

    def test_make_child(self):
        child = self.node.make_child(self.problem, self.action)
        new_state = (5,1,1,1,1,1,1,1)
        self.assertTrue(isinstance(child, Node))
        self.assertTrue(isinstance(child.path_cost, int))
        self.assertEqual(child.path_cost, 1)
        self.assertEqual(child.state, new_state)
        self.assertEqual(child.parent.state, self.node.state)
    
    def test_repr(self):
        expected = '<Node: (1, 1, 1, 1, 1, 1, 1, 1)>'
        self.assertEqual(repr(self.node), expected)
        
    def test_solution(self):
        child = self.node.make_child(self.problem, self.action)
        new_action = (7,5)
        child = child.make_child(self.problem, new_action)
        solution = child.solution()
        expected = [(0,5), (7,5)]
        self.assertTrue(isinstance(solution, list))
        self.assertEqual(len(solution),2)
        self.assertEqual(solution, expected)

    def test_hash(self):
        expected = hash(self.state)
        self.assertEqual(expected,hash(self.node))

    def test_f(self):
        initial_state = [2,2,3,4,5,6,7,7]
        end_state = (1,2,3,4,5,6,7,8) # 28 attacks

        node = Node(initial_state)
        actions = [(0,1), (7,8)]

        for action in actions:
            child = node.make_child(self.problem, action)
            node = child

        # test the for statement above
        self.assertEqual(child.state, end_state) 
        self.assertEqual(child.path_cost, 2)

        expected = 30 # 28 heuristic + 2 actions
        observed = child.f(self.problem)
        self.assertEqual(expected, observed)

if __name__ == "__main__":
    unittest.main()    
