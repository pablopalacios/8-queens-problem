#!/usr/bin/python
import unittest
from problem import a_star_search, Problem

class AStarTestCase(unittest.TestCase):

    def setUp(self):
        self.initial_state = (1,6,8,2,7,1,3,4) # it needs just one movement to win
        self.goal_state = (4,6,8,2,7,1,3,5) # 0 attack
        self.problem = Problem(self.initial_state)
        
    def test_a_star_search(self):
        solution = a_star_search(self.problem)
        self.assertEqual(len(solution), 2)

if __name__ == "__main__":
    unittest.main()
