#!/usr/bin/python
import unittest
from problem import make_initial_state, Problem

class ProblemTestCase(unittest.TestCase):
    def setUp(self):
        self.state = (0,0,0,0,0,0,0,0)

    def test_make_initial_state(self):
        observed = make_initial_state()
        self.assertEqual(len(observed),8) # check if all queens were computed
        self.assertEqual(len(set(observed)),8) # check if no value is repeated

    def test_problem_actions(self):
        """ Tests initialization and Problem.actions() """
        problem = Problem(self.state)
        self.assertEqual(problem.initial_state, self.state)
        actions = problem.actions(self.state)
        possibilities = 7 * 8 # 7 possibilidades para cada coluna
        self.assertEqual(len(actions), possibilities)
        self.assertEqual(len(set(actions)), possibilities)
        for column, row in actions:
            self.assertTrue(row!=0)
            self.assertTrue(row<=7 and row>=1)
            self.assertTrue(column>=0 and column<=7)

    def test_movement(self):
        """ Tests problem.result() """
        problem = Problem(self.state)
        action = (0,7)
        expected = (7,0,0,0,0,0,0,0)
        observed = problem.result(self.state, action)
        self.assertEqual(observed, expected)

        actions = ((i,5) for i in range(8))
        state = self.state
        for action in actions:
            observed = problem.result(state, action)
            state = observed
        expected = (5,5,5,5,5,5,5,5)
        self.assertEqual(observed,expected)

    def test_is_goal(self):
        """ Tests problem.is_goal() """
        state = (4,6,8,2,7,1,3,5)
        problem = Problem(state)
        self.assertTrue(problem.is_goal(state))
        problem = Problem(self.state)
        self.assertFalse(problem.is_goal(self.state))

    def test_path_cost(self):
        """ Tests problem.step_cost()
        Still need to test all movements possibilities """
        problem = Problem(self.state)
        action = (0,5)
        cost = problem.step_cost(self.state,action)
        self.assertEqual(cost, 1)
            
            
if __name__ == "__main__":
    unittest.main()
