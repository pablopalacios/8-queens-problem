#!/usr/bin/python
import unittest
from heuristic import horizontal_attacks, diagonal_attacks, h

class HeuristicTestCase(unittest.TestCase):

    def test_horizontal_attacks(self):
        states = ([1,3,4,5,6,7,8,1], # 1 attack
                  [1,3,4,5,1,7,8,1], # 3 attacks
                  [1,1,4,5,1,7,8,1], # 6 attacks
                  [1,2,3,4,5,6,7,8], # 0 attacks
                  )
        results = (1, 3, 6, 0)
        for state,result in zip(states,results):
            observed = horizontal_attacks(state)
            expected = result
            self.assertEqual(observed,expected)

    def test_diagonal_attacks(self):
        states = ([1,1,1,1,1,1,1,1], # 0 attack
                  [1,2,4,6,8,2,4,6], # 1 attack
                  [1,2,3,5,7,1,1,5], # 3 attacks
                  [1,2,3,4,5,6,7,8], # 28 attacks
                  )
        results = (0, 1, 3, 28)
        for state,result in zip(states,results):
            observed = diagonal_attacks(state)
            expected = result
            self.assertEqual(observed,expected)

    def test_h(self):
        """ Test heurist function for 8-queens puzzle """
        states = ([4,3,2,5,4,3,2,3], # 17 attacks
                  [1,6,2,5,7,4,8,3], # 1 attack
                  [4,6,8,2,7,1,3,5], # 0 attack
                  )
        results = (17, 1, 0)
        for state,result in zip(states,results):
            observed = h(state)
            expected = result
            self.assertEqual(observed,expected)
            
if __name__ == "__main__":
    unittest.main()
