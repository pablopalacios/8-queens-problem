#!/usr/bin/python
from tkinter import *
from tkinter.messagebox import showinfo
from problem import Problem, make_initial_state, a_star_search, hill_climbing_search, simulated_annealing_search
from time import sleep

ALL = N+S+W+E
QUEEN = 'q'

class Application(Frame):
    def __init__(self, name, problem, master=None):
        
        Frame.__init__(self, master)
        self.name = name
        self.start, self.actions, self.is_goal, self.total_nodes = problem
        self.board = [[0 for i in range(8)] for i in range(8)]
        self.config(padx=10, pady=10, borderwidth=1)
        self.pack(side=LEFT)
        self.create_widgets()

    def str_actions(self):
        return ' '.join([str((x+1, y)) for x,y in self.actions])

    def create_widgets(self):
        self.create_board()
        b = Button(self, text=self.name, pady=10, command=self.solve)
        b.grid(row=9, column=0, columnspan=8)

    def create_board(self):
        for i in range(8):
            for j in range(8):
                if divmod(j,2)[1] == divmod(i,2)[1]:
                    color = "#eee"
                else:
                    color = "green"
                b = Button(self)
                b.config(background=color,
                         borderwidth=0,
                         highlightthickness=0,
                         activebackground="gray",
                         height=1,
                         width=1,
                         font=('chess',22),
                        )
                b.grid(row=i, column=j,sticky=ALL)
                self.board[i][j] = b
        
        # put queens on board
        for column, row in enumerate(self.start):
            row = 8 - row
            self.board[row][column]['text'] = QUEEN

    def solve(self):
        for r,q in self.actions:
            row = 8-q
            column = r
            for i in range(8):
                b = self.board[i][column]
                b['text'] = ''
                b.update()
            b = self.board[row][column]
            b['text'] = QUEEN
            b.update()
            sleep(1)
        showinfo("Search Information", """{0.name}:
- Estado inicial: {0.start}        
- Nodos visitatos: {0.total_nodes}
- Quantidade de nodos da solução: {1}
- Caminho: {3}
- Rainhas atacadas: {0.is_goal}
- Resolvido: {2}""".format(self, len(self.actions), not bool(self.is_goal), self.str_actions()))

def make_problem(f, initial_state):
    problem = Problem(initial_state)
    actions, nodes = f(problem)
    state= initial_state
    for action in actions:
        state = problem.result(state, action)
    is_goal = problem.heuristic(state)
    return initial_state, actions, is_goal, nodes

if __name__ == "__main__":
    initial_state = make_initial_state()
    astar_problem = make_problem(a_star_search, initial_state)
    hill_problem = make_problem(hill_climbing_search, initial_state)
    annel_problem = make_problem(simulated_annealing_search, initial_state)

    root = Tk()
    app1 = Application('A* Search', astar_problem, master=root,)
    app2 = Application('Hill Climbing Search', hill_problem, master=root)
    app3 = Application('Simulated Annealing Search', annel_problem, master=root)
    root.mainloop()
