#!/usr/bin/python
"""
8 Queens puzzle problem and search algorithms
"""
import random
import heapq
from math import exp
from heuristic import h

def make_initial_state():
    """ Must return a list with no queen attacking other horizontally or vertically """
    state = list(range(1,9))
    random.shuffle(state)
    return tuple(state)

class Problem:
    """ Cada problema deverá ter 5 características:
    - Estado inicial
    - Função que retorna as possíveis ações para um determinado estado
    - Função que retorna um novo estado quando uma ação é aplicada
    - Função que testa se um dado estado é o estado meta
    - Função que calcula o custo de uma ação
    """
    def __init__(self, initial_state):
        self.initial_state = initial_state

    def actions(self, state):
        """ Return the possible actions to given state for a
        complete formulation problem.
        Return a list with tuples where t[0] equals the column and
        t[1] equals to row.
        """
        rows = set(range(1,9))
        actions = []
        for column, row in enumerate(state):
            queen_actions = rows - {row}
            for queen_action in queen_actions:
                actions.append((column, queen_action))
        return actions
    
    def result(self, state, action):
        """ Apply action on state and return a new state """
        new_state = list(state) # copy state to a new name
        new_state[action[0]] = action[1]
        return tuple(new_state)

    def is_goal(self, state):
        """ Return true if state is goal or false if it is not """
        if self.heuristic(state) == 0:
            return True
        return False

    def step_cost(self):
        """ Evaluates and return the cost of a given action.
        Since there is no true cost in moving one queen to another
        place, all the movements has the same cost 1 """
        return 1

    def heuristic(self, state):
        return h(state)

class Node:
    def __init__(self, state, path_cost=0, parent=None, action=None):
        """
        state : the state in the state space to which the node corresponds;
        parent : the node in the search tree that generated this node;
        action : the action that was applied to the parent to generate the node;
        path-cost : the cost, traditionally denoted by g(n), of the path from the initial state
        to the node, as indicated by the parent pointers.
        """
        self.state = state
        self.parent = parent
        self.action = action
        self.path_cost = path_cost

    def make_child(self, problem, action):
        state = problem.result(self.state,action)
        parent = self
        path_cost = self.path_cost + problem.step_cost()
        child_node = Node(state, path_cost, parent, action)
        return child_node
        
    def solution(self):
        """ Return the sequence of actions until self.node """
        path_sequence = []
        node = self
        while node.action:
            path_sequence.append(node.action)
            node = node.parent
        path_sequence.reverse()
        return path_sequence

    def f(self, problem):
        return self.path_cost + problem.heuristic(self.state)
    
    def __hash__(self):
        return hash(self.state)

    def __repr__(self):
        return '<Node: {}>'.format(self.state)

def a_star_search(problem):
    node = Node(state=problem.initial_state,path_cost=0)
    frontier = [(0,0,node)]
    explored = set()
    i = 0 # para desempatar nodes com f() iguais
    while frontier:
        g, o, node = heapq.heappop(frontier)
        if problem.is_goal(node.state):
            return node.solution(), i
        explored.add(node)

        for action in problem.actions(node.state):
            child = node.make_child(problem, action)
            if child not in explored or child not in frontier:
                heapq.heappush(frontier, (child.f(problem), i, child))
                i+=1
            elif child.state in frontier:
                node_index = frontier.index(child)
                if frontier[node_index].path_cost > child.path_cost:
                    frontier[node_index] = child

def schedule(t, k=40, lam=0.0005, limit=200):
    if t < limit:
        return k*exp(-lam*t)
    else:
        return 0

def simulated_annealing_search(problem):
    current = Node(problem.initial_state)
    t = 0
    moves = []
    while True:
        T = schedule(t)
        if not T or not problem.heuristic(current.state):
            return moves, t
        action = random.choice(problem.actions(current.state))
        next = current.make_child(problem, action)
        delta_e = problem.heuristic(next.state) - problem.heuristic(current.state)
        if delta_e <= 0 or exp(delta_e/T) < random.uniform(0.0, 1.0):
            current = next
            moves.append(action)
        t+=1

def hill_climbing_search(problem):
    current = Node(problem.initial_state)
    tested = 0
    moves = []
    while True:
        before = current
        for action in problem.actions(current.state):
            tested+=1
            child = current.make_child(problem, action)
            if problem.heuristic(child.state) < problem.heuristic(current.state):
                moves.append(action)
                current = child
        if current == before:
            return moves, tested

if __name__ == "__main__":
    #initial_state = (3, 2, 7, 6, 1, 4, 8, 5)
    #initial_state = (8, 4, 3, 6, 2, 7, 5, 1)
    initial_state = make_initial_state()
    problem = Problem(initial_state)
    print("start board: ",initial_state)

    result_astar = a_star_search(problem)
    print("A* Moves: ", result_astar)

    result_hill = hill_climbing_search(problem)
    print("Hill moves:", result_hill)

    result_annel = simulated_annealing_search(problem)
    print("Annel moves:", result_annel)
